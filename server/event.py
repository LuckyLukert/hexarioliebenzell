#!/usr/bin/env python3.7
# coding=utf-8

from geometry import *
from player import *
from settings import *
import logging


def connectedEvent(event, game): logging.debug("connectedEvent: "+str(event.sender))
def playerJoinedEvent(event, game): logging.debug("playerJoinedEvent: "+str(event.sender))
def playerLeftEvent(event, game): logging.debug("playerLeftEvent: "+str(event.sender))
def gameStateEvent(event, game): logging.debug("gameStateEvent: "+str(event.sender))


def disconnectedEvent(event, game):
    logging.debug("disconnectedEvent: "+str(event.sender))
    if game.removePlayer(event.sender):
        game.server.broadcast(Event({"event":"playerLeft", "id": event.sender}))

def getConnectedEvent(game, id):
    conDict = {"event": "connected"}
    conDict["id"] = id
    conDict["boardSize"] = BOARD_SIZE
    conDict["cellRadius"] = CELL_RADIUS
    conDict["playerSize"] = PLAYER_SIZE
    conDict["strokeSize"] = STROKE_SIZE
    conDict["ownership"] = game.board.ownership
    return Event(conDict)
    

def connectObserverEvent(event, game):
    logging.debug("connectObserverEvent: "+str(event.sender))
    game.server.send(event.sender, getConnectedEvent(game, event.sender))

    for (id, player) in game.board.players.items():
        game.server.send(event.sender, Event(player.reprJSONjoin()))

    
def connectEvent(event, game):
    logging.debug("connectEvent: "+str(event.sender))
    if not hasattr(event, "name"):
        event.__dict__["name"] = "Egal, einfach Peter"

    newPlayer: Player = game.addPlayer(event.sender, event.name)
    game.server.send(event.sender, getConnectedEvent(game, event.sender))
    
    #send each playerJoin to himself only
    for (id, player) in game.board.players.items():
        if id == event.sender:
            continue
        game.server.send(event.sender, Event(player.reprJSONjoin()))
    
    game.server.broadcast(Event(newPlayer.reprJSONjoin()))

def rotateEvent(event, game):
    logging.debug("rotateEvent: " + str(event.sender))
    if not hasattr(event, "angle"):
        logging.error("no angle given")
        return
    game.board.players[event.sender].changeRotation(float(event.angle))


def goDirectionEvent(event, game):
    logging.debug("goDirectionEvent: " + str(event.sender))
    if not hasattr(event, "dx") or not hasattr(event, "dy"):
        logging.error("no direction given")
        return
    if event.dx == 0 and event.dy == 0:
        logging.error("direction was 0")
        return
    game.board.players[event.sender].changeDirection(Vector(float(event.dx), float(event.dy)))




eventsExecute = {
    "connected": connectedEvent,
    "playerJoined": playerJoinedEvent,
    "playerLeft": playerLeftEvent,
    "gameState": gameStateEvent,
    "disconnected": disconnectedEvent,
    "connectObserver": connectObserverEvent,
    "connect": connectEvent,
    "rotate": rotateEvent,
    "goDirection": goDirectionEvent,
    "test": lambda event, game: logging.info("Test Event from "+str(event.sender)+": " + event.message)
}




class Event:
    def __init__(self, entries, sender=None):
        self.event = None
        self.sender = sender
        self.__dict__.update(entries)

    def execute(self, game):
        try:
            with game.lock:
                eventsExecute[self.event](self, game)
        except KeyError:
            pass

    def reprJSON(self):
        enc = self.__dict__.copy()
        enc.pop("sender")
        return enc
