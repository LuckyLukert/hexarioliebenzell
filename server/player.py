#!/usr/bin/env python3.7
# coding=utf-8

import random
import tkinter
from typing import List
from typing import Set
from settings import *
from geometry import *
from hexlib import *
import logging
from board import *


class Player:
    def __init__(self,  id: int, pos: Point = Point(0,0), name: str = "", color: str = None):
        self.pos: Point = pos
        self.direction: Vector = Vector(1,0)*PLAYER_SPEED
        self.rotation: float = 0
        self.tail: List[Point] = []
        self.tailHex: List[HexPoint] = []
        self.id: int = id
        self.name: str = name
        
        self.color: str = color
        if color is None:
            self.color = random.choice(COLORS)
        self.canvas = None
        self.transformation = None
        self.graphics = None
    
    def changeRotation(self, delta: float):
        self.rotation = MAX_ROTATION*max(min(delta, 1), -1)
        

    def changeDirection(self, direction: Vector):
        angle = -math.atan2(direction.x * self.direction.y - direction.y * self.direction.x,
                            direction.x * self.direction.x + direction.y * self.direction.y)
        self.rotation = max(min(angle, MAX_ROTATION), -MAX_ROTATION)
    
    
    def addTail(self, board: Board):
        hex: HexPoint = HexPoint.fromPoint(self.pos)
        drawTailHex: bool = False
        if (board[hex] != self.id):
            self.tail.append(self.pos)
            if len(self.tailHex)==0 or self.tailHex[-1].getDirection(hex) != -1:
                if not hex in self.tailHex:
                    drawTailHex = True
                self.tailHex.append(hex)
            
            #only for representation on board
            if  self.transformation is not None and self.canvas is not None and self.graphics is not None:
                if len(self.tail) > 1:
                    a = self.tail[-2].transform(self.transformation)
                    b = self.tail[-1].transform(self.transformation)
                    self.canvas.create_line(a.x, a.y, b.x, b.y, width=STROKE_SIZE * PLAYER_SIZE * self.transformation.scalex, fill=self.color, capstyle=tkinter.ROUND, tag="tail")
                if drawTailHex:
                    hex.drawHexagon(CELL_RADIUS, self.color, False, STROKE_SIZE * PLAYER_SIZE * self.transformation.scalex / 7, self.canvas, self.transformation)

    def move(self):
        self.direction = self.direction.rotate(self.rotation)
        self.rotation = 0
        self.pos += self.direction

        #only for representation on board
        if self.transformation is not None and self.canvas is not None and self.graphics is not None:
            movement = self.direction.transform(self.transformation)
            self.canvas.move(self.graphics, movement.x, movement.y)
        
    
    
    def checkComplete(self, board: Board)->List[HexPoint]:
        if len(self.tailHex) == 0:
            return []

        hex: HexPoint = HexPoint.fromPoint(self.pos)
        if (board[hex] != self.id):
            return []
        
        
        border: List[Set[HexPoint]] = [{hex},{hex}]
        
        for rightIsNeg in [-1,1]:  #try left way and right way
            current: HexPoint = hex
            dir: int = self.tailHex[-1].getDirection(current)
            if dir == -1:
                logging.error("checkComplete: position not adjacent to tailHex")
                border[(rightIsNeg + 1) // 2] = set()
                continue
            dir -= rightIsNeg  #rotate for start
            
            while True:
                finished: bool = False
                for i in range(-1, 4):  #4 excluded
                    i *= rightIsNeg
                    nextPoint = current.goIntoDirection(dir+i)
                    if (board.isOutside(nextPoint)):
                        continue
                    if nextPoint == self.tailHex[0]:  #found end
                        finished = True
                        break
                    if nextPoint == hex:  #found loop
                        finished = True
                        border[(rightIsNeg+1)//2] = set()
                        break
                    if (board[nextPoint] == self.id):  #found next
                        current = nextPoint
                        border[(rightIsNeg+1)//2].add(current)
                        dir = dir+i
                        break
                else:  #found nothing (only if player hit single field
                    border[(rightIsNeg+1)//2] = set()
                    break
                if finished:
                    break
        
        #fill in border and tailHex
        toFill: Set[HexPoint] = border[0]
        if len(border[0]) > len(border[1]):
            toFill = border[1]
        result: Set[HexPoint] = board.fillInterior(set(self.tailHex) | toFill, self.id)
        
        self.tailHex = []
        self.tail = []
        
        return list(result)
    
    
    
    def touchesTail(self, pos: Point) -> bool:
        if (self.pos - pos).length2() < 0.000001:
            return False
        
        if (self.pos - pos).length2() <= PLAYER_SIZE:
            return True
        
        i: int = 0
        while i < len(self.tail):
            dist: int = (self.tail[i]-pos).length()
            if dist < (0.5*PLAYER_SIZE*(1+STROKE_SIZE)):
                return True
            else:
                i += max(1, int(dist/PLAYER_SPEED)-1)
        return False
    
    
    def die(self, board: Board):
        for line in board.ownership:
            for index, content in enumerate(line):
                if content == self.id:
                    line[index] = -1
        self.tail = []
        self.tailHex = []
    
    
    def spawn(self, board: Board) -> HexPoint:
        safetyCounter: int = 0
        while True:
            safetyCounter+=1
            x:int = random.randint(0,board.size*2-2)
            y:int = random.randint(0,board.size*2-2)
            fieldsAround: List[HexPoint] = [HexPoint(x,y).goIntoDirection(i) for i in range(0,6)]
            fieldsAround.append(HexPoint(x,y))
            
            for field in fieldsAround:
                if board.isOutside(field):
                    break
                if board[field] != -1 and safetyCounter < 1000:
                    break
            else:  #in case of no break
                self.pos = HexPoint(x,y).getPoint(0,CELL_RADIUS,CELL_RADIUS)
                for field in fieldsAround:
                    board[field] = self.id
                return HexPoint(x,y)
    
    
    
    def redraw(self, canvas, trans):
        self.canvas = canvas
        self.transformation = trans
        upperLeft = (self.pos - Vector(PLAYER_SIZE/2, PLAYER_SIZE/2)).transform(trans)
        lowerRight = (self.pos + Vector(PLAYER_SIZE/2, PLAYER_SIZE/2)).transform(trans)
        coord = upperLeft.x, upperLeft.y, lowerRight.x, lowerRight.y
        self.graphics = canvas.create_oval(coord, fill=self.color, tag="player", outline="gray90", width=5)
        
        for field in self.tailHex:
            field.drawHexagon(CELL_RADIUS, self.color, False, STROKE_SIZE * PLAYER_SIZE * self.transformation.scalex / 7, self.canvas, trans)
        
        if len(self.tail)>3:
            self.canvas.create_line(*[(p.transform(trans).x,p.transform(trans).y) for p in self.tail], width=STROKE_SIZE * PLAYER_SIZE * self.transformation.scalex, fill=self.color, capstyle=tkinter.ROUND, tag="tail")

    def reprJSONstep(self):
        rep = dict()
        rep["id"] = self.id
        rep["x"] = self.pos.x
        rep["y"] = self.pos.y
        rep["newTail"] = len(self.tail) > 0
        return rep

    def reprJSONjoin(self):
        rep = {"event": "playerJoined"}
        rep["id"] = self.id
        rep["name"] = self.name
        rep["color"] = self.color
        rep["x"] = self.pos.x
        rep["y"] = self.pos.y
        rep["tail"] = [{"x":p.x,"y":p.y} for p in self.tail]
        return rep


if __name__ == '__main__':
    import logging
    import sys
    logging.basicConfig(level=logging.DEBUG, format="%(levelname)s %(message)s", stream=sys.stdout)
    
    player = Player(Point(0, 0))
    player.changeRotation(0.1)
    player.move()
    player.move()
    player.move()  #5 * 0.2 movement
    player.move()
    player.move()
    logging.debug(player.reprJSON())
    logging.debug(str((player.pos - Point(0,0)).length2()) + " == 1")
    if player.pos.x < 1:
        logging.info("Player test successful")
    else:
        logging.error("Player test failed!")

