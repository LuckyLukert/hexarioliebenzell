#!/usr/bin/env python3.7
# coding=utf-8

from typing import Dict
from settings import *
from hexlib import *
from player import *


class Board:
    
    
    def __init__(self, size=BOARD_SIZE):
        self.size = size
        self.ownership: List[int] = [[-1]*(size*2-1) for _ in range(size*2-1)]
        self.players: Dict[int, Player] = dict()

    def __getitem__(self, key: HexPoint) -> int:
        return self.ownership[key.x][key.y]

    def __setitem__(self, key: HexPoint, item: int):
        self.ownership[key.x][key.y]=item
    
    
    def isOutside(self, pos: HexPoint):
        if pos.x<0 or pos.y<0 or pos.x>=(self.size*2-1) or pos.y>=(self.size*2-1):
            return True
        if (pos.y-self.size) >= pos.x:
            return True
        if (self.size+pos.y) <= pos.x:
            return True
        return False
    
    
    def fillInterior(self, border: Set[HexPoint], id: int) -> Set[HexPoint]:
        """
        fills all inner fields and the border with id
        :return: the fields modified
        """
        if len(border) == 0:
            return set()
        
        startPoint: HexPoint = HexPoint(self.size*10, self.size*10)
        for point in border:
            if point.x < startPoint.x:  #find leftmost point on border
                startPoint = point

        outerBorder: Set[HexPoint] = {startPoint.goIntoDirection(-1)}
        current: HexPoint = startPoint
        dir: int = 1
        
        finished: bool = False
        while True:
            for i in range(-1, 5):
                nextPoint = current.goIntoDirection(dir + i)
                if nextPoint in border:  #found next
                    current = nextPoint
                    dir = dir + i
                    break
                outerBorder.add(nextPoint)
            else:
                break
            if finished:
                break
            if current == startPoint:  #found end but continue one loop
                finished = True
        
        changed: Set[HexPoint] = set()
        visited: Set[HexPoint] = set()
        queue: List[HexPoint] = [startPoint]
        while len(queue) != 0:
            toPaint: HexPoint = queue.pop()
            if (self[toPaint] != id):
                changed.add(toPaint)
            visited.add(toPaint)
            self[toPaint] = id
            for i in range(0,6):
                addQueue: HexPoint = toPaint.goIntoDirection(i)
                if addQueue in outerBorder or addQueue in visited:
                    continue
                if self.isOutside(addQueue):  #should not be necessary
                    continue
                queue.append(addQueue)

        return changed
        
    
    
    def iterateFields(self):
        for y in range(0,self.size):
            for x in range(0,self.size+y):
                yield HexPoint(x,y)
        for y in range(0, self.size - 1):
            for x in range(0, self.size + (self.size - y) - 2):
                yield HexPoint(x+y+1, self.size+y)
    
    
    def redraw(self, canvas, trans):
        canvas.delete("hex")
        canvas.delete("hexstroke")
        for field in self.iterateFields():
            color: str = "grey"
            if self[field] != -1:
                color = self.players[self[field]].color
            field.drawHexagon(CELL_RADIUS, color, True, 0, canvas, trans)
        

        
    