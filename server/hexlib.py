#!/usr/bin/env python3.7
# coding=utf-8


from geometry import *
from settings import *



class HexPoint:
    def __init__(self, x: int, y: int):
        self.x:int = x
        self.y:int = y
    
    
    def getDirection(self, toPoint) -> int:
        """
        calculates the direction from self to the toPoint if adjacent
        :return: -1 if they are not adjacent
        """
        xdist: int = toPoint.x - self.x
        ydist: int = toPoint.y - self.y
        if (xdist == -1 and ydist == -1):
            return 0
        if (xdist == 0 and ydist == -1):
            return 1
        if (xdist == 1 and ydist == 0):
            return 2
        if (xdist == 1 and ydist == 1):
            return 3
        if (xdist == 0 and ydist == 1):
            return 4
        if (xdist == -1 and ydist == 0):
            return 5
        return -1
        
    
    
    def goIntoDirection(self, dir: int):
        dir %= 6
        x: int = self.x
        y: int = self.y
        if (dir == 0 or dir == 1):
            y-=1
        elif (dir == 3 or dir == 4):
            y += 1
        if (dir == 0 or dir == 5):
            x -= 1
        elif (dir == 2 or dir == 3):
            x += 1
        return HexPoint(x,y)

    @classmethod
    def fromPoint(cls, pos: Point, radius:float=CELL_RADIUS):
        """
        returns the HexPoint on which the (float-valued) pos is.
        """
        xdist:float = math.sin(math.pi/3) * radius
        y:float = (pos.y+0.5*radius) / (radius * 1.5)
        x:float = (pos.x+xdist) + (xdist * math.floor(y))
        x /= 2*xdist
        
        #all possible HexPoints that could be the result
        possible = \
            [HexPoint(int(math.floor(x)), int(math.floor(y))),
             HexPoint(int(math.floor(x)), int(math.floor(y+1))),
             HexPoint(int(math.floor(x+1)), int(math.floor(y+1)))]
        bestPoint:HexPoint
        minDist:float = math.inf
        
        #select the nearest possible HexPoint
        for p in possible:
            posVec:Vector = p.getPoint(0,radius,radius) - pos
            dist:float = posVec.length2()
            if minDist > dist:
                minDist = dist
                bestPoint = p
        return bestPoint
        
        
    
    def drawHexagon(self, radius:float, color:str, fill:bool, strokeWidth:float, canvas, trans:Transformation):
        offset = 0
        if fill:
            offset=radius*0.1
            
        line = []
        for i in range(0,7):
            point = self.getPoint(i,radius,offset)
            point = point.transform(trans)
            line.append(point.x)
            line.append(point.y)
        
        if fill:
            canvas.create_polygon(*line, tags="hex", fill=color)
        else:
            #canvas.create_polygon(*line, tags="hex", fill=color, stipple="gray25")
            num = int(color[1:],16)
            brighter = min((num//(256*256))%256 + LIGHT_COLOR_INCREASE, 255) * 256 * 256
            brighter += min((num//256)%256 + LIGHT_COLOR_INCREASE, 255) * 256
            brighter += min((num)%256 + LIGHT_COLOR_INCREASE, 255)
            canvas.create_line(*line, tags="hexstroke", width=strokeWidth, fill="#"+hex(brighter)[2:].zfill(6))
            
            #canvas.create_line(*line, tags="hexstroke", width=strokeWidth, fill="white", stipple="gray50")
            
    
    def getPoint(self, edge:int, radius:float, offset:float=0) -> Point:
        """
        returns the (float-valued) Point in space which corresponds to the HexPoint
        :param edge: in [0,5] determines which edge should be converted to a point
        :param radius: the radius of each hexagons
        :param offset: go this amount "back" to the middle point. If offset=radius, the middle point is returned.
        """
        xx = (self.x*2 - self.y)*radius*math.cos(math.pi/6)
        yy = self.y * .75
        toEdge = Vector(0,radius-offset).rotate(math.pi/3*edge)
        return Point(xx,yy) + toEdge
    

    def __eq__(self, other):
        if isinstance(other, HexPoint):
            return self.x == other.x and self.y == other.y
        return NotImplemented


    def __hash__(self):
        return hash(tuple(sorted(self.__dict__.items())))
    
    