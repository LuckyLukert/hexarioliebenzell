#!/usr/bin/env python3.7
# coding=utf-8
import socket

import time

from settings import *
from geometry import *
from game import *
from threading import Thread

class HumanClient:
    def __init__(self, game: Game):
        self.socket = None
        self.game = game
        self.id = -1
        self.mouseX = 0
        self.mouseY = 0
        
        
    def start(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(('localhost', PORT))
        self.socket.send('{"event": "connect", "name": "Human"}\n'.encode("utf-8"))
        Thread(target=self.dummy_receiver, daemon=True).start()
        for safetyCounter in range(0,100):
            for player in self.game.board.players.values():
                if player.name == "Human":
                    self.id = player.id
                    return
            time.sleep(0.01)
        
        
    def dummy_receiver(self):
        while True:
            self.sendMovement()  #do often
            try:
                self.socket.recv(1024)
            except:
                pass
    
    def sendMovement(self):
        if (self.game.transformation is None or self.socket is None):
            return
        mousePoint: Point = Point(self.mouseX, self.mouseY)
        ownPoint: Point = self.game.board.players[self.id].pos.transform(self.game.transformation)
        direction: Vector = mousePoint - ownPoint
        self.socket.send(('{"event": "goDirection", "dx": ' + str(direction.x) + ', "dy": ' + str(direction.y) + '}\n').encode("utf-8"))

    def mouseMoved(self, event):
        self.mouseX = event.x
        self.mouseY = event.y




def simpleClient(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send('{"event": "connect"}\n'.encode("utf-8"))
    while True:
        s.send('{"event": "rotate", "angle": 0.1}\n'.encode("utf-8"))
        try:
            s.recv(1024)
        except:
            pass




