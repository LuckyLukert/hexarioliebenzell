#!/usr/bin/env python3.7
# coding=utf-8

import threading

from player import *
from geometry import *
from board import *
from event import *


class Game:
    def __init__(self, server):
        self.board: Board = Board()
        
        self.server = server
        self.lock = threading.RLock()
        self.shouldRedraw = True
        self.canvas = None
        self.transformation = None
        
    
    
    def step(self):
        with self.lock:
            for player in self.board.players.values():
                player.move()
                
                if self.board.isOutside(HexPoint.fromPoint(player.pos)):  #player crushed in border
                    self.respawnPlayer(player)
                for opponent in self.board.players.values():
                    if opponent.touchesTail(player.pos) and opponent.tail != []:  #player crushed in tail of opponent
                        if player.touchesTail(opponent.pos) and player.tail != []:  #opponent crushed as well
                            self.respawnPlayer(player)
                        self.respawnPlayer(opponent)
                        break

                player.addTail(self.board)
                added: Set[HexPoint] = player.checkComplete(self.board)
                if added != []:
                    self.redraw()
                    fullEvent = {"event": "playerFull"}
                    fullEvent["id"] = player.id
                    fullEvent["hexPositions"] = [{"x":p.x, "y":p.y} for p in added]
                    self.server.broadcast(Event(fullEvent))
                    
            playerList = []
            for (id, player) in self.board.players.items():
                playerList.append(player.reprJSONstep())
            stepEvent = {"event": "step", "players": playerList}
            self.server.broadcast(Event(stepEvent))
            
    
    
    def respawnPlayer(self, player: Player):
        player.die(self.board)
        pos: HexPoint = player.spawn(self.board)
        self.redraw()
        event = {"event": "playerRespawned"}
        event["id"] = player.id
        event["hexPosX"] = pos.x
        event["hexPosY"] = pos.y
        self.server.broadcast(Event(event))
        
    
    
    def removePlayer(self, id: int) -> bool:  #true if actual Player was removed
        removed = self.board.players.pop(id)
        if removed == None:
            return False
        removed.die(self.board)
        self.redraw()
        return True
    
    
    def addPlayer(self, id: int, name: str) -> Player:
        player = Player(id, name=name)
        
        currentColors = [player.color for player in self.board.players.values()]
        for i in range(0,1000):
            color = random.choice(COLORS)
            if not color in currentColors:
                player.color = color
                break
        
        self.board.players[id] = player
        player.spawn(self.board)
        self.redraw()
        return player
    
    
    def reallyRedraw(self, canvas=None):
        with self.lock:
            if canvas is not None:
                self.canvas = canvas
            if self.canvas is None:
                return
            if not self.shouldRedraw:
                return
            self.shouldRedraw=False
            
            
            self.canvas.delete("player")
            self.canvas.delete("tail")
        
            margin = 20
            w = self.canvas.width - margin * 2
            h = self.canvas.height - margin * 2
            
            WIDTH = (BOARD_SIZE*2-1) * CELL_RADIUS * math.cos(math.pi/6)*2
            HEIGHT = (BOARD_SIZE*2-2/3) * CELL_RADIUS * 1.5
            OFFX = BOARD_SIZE * CELL_RADIUS * math.cos(math.pi/6)
            OFFY = CELL_RADIUS
            
            trans = Transformation(margin, margin)
            if (w / h > WIDTH / HEIGHT):  # height too small
                trans.scaley = h / HEIGHT
                trans.scalex = trans.scaley
                trans.addx += (w - (WIDTH * trans.scalex)) / 2
            else:  # width too small
                trans.scalex = w / WIDTH
                trans.scaley = trans.scalex
                trans.addy += (h - (HEIGHT * trans.scaley)) / 2
            trans.addx += OFFX*trans.scalex
            trans.addy += OFFY*trans.scaley
            
            self.transformation = trans
            self.board.redraw(self.canvas, trans)
            
            for player in self.board.players.values():
                player.redraw(self.canvas, trans)
    
    
    def redraw(self):
        with self.lock:
            self.shouldRedraw = True
        
    
    
    def reprJSON(self):
        rep = dict()
        playerList = []
        for (id, player) in self.board.players.items():
            playerRep = player.reprJSON()
            playerRep["id"] = id
            playerList.append(playerRep)
        rep["players"] = playerList
        return rep

    
    
    
    
        
        