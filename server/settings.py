#!/usr/bin/env python3.7
# coding=utf-8

import logging


#Network
IP = "localhost"
PORT = 1893

#sizes
PLAYER_SIZE = 1  #diameter of the player
STROKE_SIZE = 0.3  #of player size
CELL_RADIUS = 0.5
BOARD_SIZE = 15

#movement
PLAYER_SPEED = 0.1
MAX_ROTATION = 0.5

COLORS = ["#FFD35C", "#FFD35C", "#F37252", "#FF92A5", "#EA3E70", "#8A9747", "#4BC4D5", "#0180B5", "#954567", "#C62C3A"]
LIGHT_COLOR_INCREASE = 50
#COLORS = ["red", "blue", "sea green", "orange", "blue violet", "HotPink2", "gold2"]

#timing (increase for debugging)
STEP_TIMING = 40

#start settings (are overwritten by command line options)
DEBUG_LEVEL = logging.INFO
START_GUI = True
HUMAN_AMOUNT = 1  #1 or 0
DUMMY_AMOUNT = 3
