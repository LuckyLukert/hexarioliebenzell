#!/usr/bin/env python3.7
# coding=utf-8
import logging
import signal
import threading
import sys
import getopt

import time

from client import HumanClient, simpleClient
from game import Game
from network import ServerThread
from settings import *
from gui import Gui




if __name__ == '__main__':
    argv = sys.argv[1:]
    logging.basicConfig(level=DEBUG_LEVEL, datefmt="%H:%M:%S", format="%(asctime)s-%(levelname)s %(message)s", stream=sys.stdout)

    port = PORT
    ip = IP
    startGui = START_GUI
    humanAmount = HUMAN_AMOUNT
    dummyAmount = DUMMY_AMOUNT
    helpMessage = "main.py -h (help) -i <ip> -p <port> -g (to not start gui) -c <humanClientAmount <= 1> -d <dummyClientAmount>"
    try:
        opts, args = getopt.getopt(argv, "hi:p:gc:d:")
    except getopt.GetoptError:
        logging.critical(helpMessage)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            logging.info(helpMessage)
            sys.exit()
        if opt == '-i':
            ip = arg
        if opt == '-p':
            port = int(arg)
        if opt == '-g':
            startGui = False
            if humanAmount != 0:
                humanAmount = 0
                logging.info("human player amount set to 0")
        if opt == '-c':
            if not startGui:
                if humanAmount != 0:
                    humanAmount = 0
                    logging.info("human player amount set to 0")
            else:
                humanAmount = int(arg)
        if opt == '-d':
            dummyAmount = int(arg)
        
    
    if not startGui and humanAmount >= 1:
        logging.critical("no gui but humanAmount\naborting...")
        sys.exit(2)
    
    
    server = ServerThread()
    game = Game(server)


    if humanAmount > 0:
        from client import HumanClient

    gui: Gui = None
    if startGui:
        human = HumanClient(game)
        gui = Gui(game, human)
        
    server.start(game, ip, port)
    time.sleep(0.5)
    
    
    
    def startSimpleClient():
        simpleClient(ip, port)
    for i in range(0,dummyAmount):
        threading.Thread(daemon=True, target=startSimpleClient).start()
    
    if humanAmount > 0:
        human.start()

    # terminates server in case of abort
    def signal_handler(signal, frame):
        server.terminate()
        time.sleep(0.5)
        logging.info("server terminated")
        sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    
    
    if startGui:
        gui.start()
    else:
        logging.info("press Ctrl+C to terminate the server")
        while True:
            time.sleep(STEP_TIMING/1000)
            game.step()
    
    server.terminate()
    time.sleep(0.5)
    logging.info("server terminated")