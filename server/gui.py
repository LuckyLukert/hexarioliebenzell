#!/usr/bin/env python3.7
# coding=utf-8

from tkinter import *

from client import *
from settings import *
from game import Game



class GameCanvas(Canvas):
    def __init__(self, parent, game :Game, **kwargs):
        Canvas.__init__(self, parent, **kwargs)
        self.bind("<Configure>", self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()
        self.game :Game = game

    def on_resize(self,event):
        self.width = event.width
        self.height = event.height
        self.config(width=self.width, height=self.height)
        self.game.redraw()




class Gui:
    def __init__(self, game, human: HumanClient):
        self.top = Tk()
        self.canvas = GameCanvas(self.top, game, bg="grey10")
        self.top.bind('<Motion>', human.mouseMoved)
    
    def start(self):
        self.canvas.pack(fill=BOTH, expand=YES)
        self.canvas.update()
        self.step()
        self.top.mainloop()
    
    def step(self):
        self.canvas.after(STEP_TIMING, self.step)
        self.canvas.game.step()
        self.canvas.game.reallyRedraw(self.canvas)



if __name__ == '__main__':
    game = Game(None)
    game.addPlayer(0,"Philip")
    game.addPlayer(1,"Hexar")
    game.players[0].changeRotation(1)
    gui = Gui(game, [])
    gui.start()
    
    
    
    
    
    
    
    
    