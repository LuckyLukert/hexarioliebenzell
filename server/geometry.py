#!/usr/bin/env python3.7
# coding=utf-8

import math

class Transformation:
    def __init__(self, addx:float=0, addy:float=0, scalex:float=0, scaley:float=0):
        self.addx:float = addx
        self.addy:float = addy
        self.scalex:float = scalex
        self.scaley:float = scaley
        
    
    

class Vector:
    def __init__(self, x :float, y :float):
        self.x = x
        self.y = y
        
    def __add__(self, other):
        return Vector(self.x +other.x, self.y +other.y)
    
    def __mul__(self, scalar :float):
        return Vector(self.x *scalar, self.y *scalar)

    def normalize(self):
        newVector = Vector(self.x, self.y)
        length = newVector.length()
        newVector.x /= length
        newVector.y /= length
        return newVector
    
    def transform(self, transformation :Transformation):
        newVector = Vector(self.x, self.y)
        newVector.x *= transformation.scalex
        newVector.y *= transformation.scaley
        return newVector
    
    def rotate(self, angle: float):
        x = self.x * math.cos(angle) - self.y * math.sin(angle);
        y = self.x * math.sin(angle) + self.y * math.cos(angle);
        return Vector(x,y)
    
    def cut(self):
        length2 = self.length2()
        if length2 > 1:
            return self.normalize()
        return Vector(self.x, self.y)
        
    
    def length2(self):
        return self.x*self.x +self.y*self.y
    
    def length(self):
        return self.length2() ** 0.5
    
    @classmethod
    def byJSON(cls, entries):
        v = cls(0, 0)
        v.__dict__.update(entries)
        return v
    
    def reprJSON(self):
        return self.__dict__.copy()
    

    def __eq__(self, other):
        if isinstance(other, Vector):
            return self.x == other.x and self.y == other.y
        return NotImplemented


    def __hash__(self):
        return hash(tuple(sorted(self.__dict__.items())))


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y
    
    def __add__(self, other: Vector):
        return Point(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        if isinstance(other, Vector):
            return Point(self.x - other.x, self.y - other.y)
        else:
            return Vector(self.x - other.x, self.y - other.y)
    
    def __mul__(self, scalar: float):
        return Point(self.x * scalar, self.y * scalar)

    def transform(self, transformation :Transformation):
        newPoint = Point(self.x, self.y)
        newPoint.x *= transformation.scalex
        newPoint.y *= transformation.scaley
        newPoint.x += transformation.addx
        newPoint.y += transformation.addy
        return newPoint

    @classmethod
    def byJSON(cls, entries):
        v = cls(0, 0)
        v.__dict__.update(entries)
        return v
    
    def reprJSON(self):
        return self.__dict__.copy()
    

    def __eq__(self, other):
        if isinstance(other, Point):
            return self.x == other.x and self.y == other.y
        return NotImplemented


    def __hash__(self):
        return hash(tuple(sorted(self.__dict__.items())))
    
    
    
if __name__ == '__main__':
    import logging
    import sys
    logging.basicConfig(level=logging.DEBUG, format="%(levelname)s %(message)s", stream=sys.stdout)
    logging.debug((Point(3,3)+Vector(2,5)).reprJSON())
    logging.debug(Vector.byJSON({'x': 5, 'y': 8}).cut().length2())
    logging.debug(Vector(1,0).rotate(3.1415/2).reprJSON())