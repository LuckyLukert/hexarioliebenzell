# hexarioLiebenzell

![Screenshot](https://gitlab.com/LuckyLukert/hexarioLiebenzell/raw/master/screenshot.png)



## Hintergrund
Der BWINF Bieber liebt Videospiele. Sein Lieblingsspiel [hexar.io](https://hexar.io) kann er im Browser gegen Spieler aus aller Welt spielen. Allerdings ist er sehr vorsichtig geworden, was das ausführen von fremdem Javascript-code in seinem Browser angeht. Deshalb hat er jetzt jegliches Javascript blockiert. Leider funktioniert hexar.io dadurch nicht mehr. Deshalb hat er mich gefragt, ob ich das Spiel nochmal für ihn implementieren kann. Bis jetzt habe ich aber erst einen Server programmiert, der es einem einzelnen Spieler ermöglicht, hexar.io zu spielen. Jedoch macht es nicht viel Spaß, ein Multiplayer-Spiel alleine zu spielen...

Könntet ihr mir helfen einen Client zu schreiben, damit wir am Ende zusammen spielen können? Ihr könnt jede Programmiersprache verwenden, die ihr wollt. Mit dem Server, dessen python-code hier im Repository ist, wird über TCP per JSON kommuniziert. Ihr werdet also im Laufe des Projektes mit Bibliotheken für TCP, JSON und einem GUI zu tun haben.




## Nachrichten zum Server
Die folgenden Nachrichten nimmt der Server als Spielzüge an:
- connect(name)
- connectObserver()  // wenn ihr nur zuschauen wollt
- rotate(angle∈[-1,1])  // die Ausrichtung eures Spielers um angle*MAX_ROTATION ändern
- goDirection(dx, dy)  // in diese Richtung ausrichten (so weit wie möglich bei MAX_ROTATION)

Ihr könnt entweder rotate oder goDirection verwenden.


## Nachrichten vom Server
Diese Nachrichten werdet ihr vom Server empfangen:
- connected(id, boardSize, cellRadius, playerSize, strokeSize, ownership) // Start
- playerJoined(id, name, color, x, y, tail∈[(x,y)]) // Füge den Spieler an dieser Position hinzu
- playerLeft (id) // Lösche alle Felder dieses Spielers
- playerRespawned(id, hexPosX, hexPosY) // Lösche die alten Felder des Spielers und füge die neuen 7 hinzu. Setze die Position.
- playerFull(id, fields) // Entferne die Abgrenzungslinielinie des Spielers und füge die neu gewonnenen Felder hinzu
- step([{id, x, y, newTail}]) // Bewege alle Spieler auf ihre neue Position und verlängere ihre Abgrenzungslinie wenn newTail==true.

Wenn eine hexPosX/Y-Position (Ganzzahlen) angegeben wird, ist damit ein Feld auf dem Spielplan gemeint. Andere Positionen (Fließkommazahlen) sind exakte Punkte auf dem Spielfeld.


## Beispiele für Nachrichten
Die JSON-Nachrichten sehen wie folgt aus:
### goDirection
```json
{
	"event": "goDirection",
	"dx": -7.2,
	"dy": -6.0
}
```

### connected
```json
{
	"event": "connected",
	"id": 0,
	"boardSize": 2,
	"cellRadius": 0.5,
	"playerSize": 1,
	"strokeSize": 0.3,
	"ownership": [[0, 0, -1], [0, 0, 0], [-1, 0, 0]]
}
```
### step
```json
{
	"event": "step",
	"players": [
		{
			"id": 0,
			"x": 4.546187404415073,
			"y": 3.4080131370281648,
			"newTail": true
		}
	]
}
```



## TODO
- Namen der Spieler anzeigen
- Geschwindigkeits-Futter hinzufügen

	
## Installation
- installiere python 3.7 und tkinter
- führe main.py aus

## Cheat-Sheet für sechseckige Felder
![Hexagons](https://gitlab.com/LuckyLukert/hexarioLiebenzell/raw/master/hexagons.png)
